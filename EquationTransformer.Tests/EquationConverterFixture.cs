﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using EquationTransformer.Parsers;
using EquationTransformer.Processors;
using EquationTransformer.Tokenizer;
using NUnit.Framework;

namespace EquationTransformer.Tests
{
    [TestFixture]
    public class EquationConverterFixture
    {
        private EquationConverter _target;

        [SetUp]
        public void SetUp()
        {
            var processors = new List<IEquationProcessor>()
                             {
                                 new PartMoveProcessor(), 
                                 new SummandReducerProcessor(), 
                             }
            ;
            _target = new EquationConverter(
                    new EquationParser(
                        new TokenReader(CultureInfo.InvariantCulture), CultureInfo.InvariantCulture),
                    processors);
        }

        [Test]
        [TestCase("x^2 + 3.5xy + y = y^2 - xy + y", "x^2-y^2+4.5xy=0")]
        public void ParseAndProcessEquation(string given, string expected)
        {
            Console.WriteLine(given);
            var actual = _target.ConvertToCanonical(given);
            Console.WriteLine(actual);
            Assert.AreEqual(expected, actual);
        }
    }
}