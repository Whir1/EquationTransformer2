﻿using System;
using System.Globalization;
using EquationTransformer.Model;
using EquationTransformer.Parsers;
using EquationTransformer.Tokenizer;
using NUnit.Framework;

namespace EquationTransformer.Tests.Parsers
{
    [TestFixture]
    public class EquationParserFixture
    {
        private EquationParser _target;

        [SetUp]
        public void SetUp()
        {
            _target = new EquationParser(new TokenReader(CultureInfo.InvariantCulture), CultureInfo.InvariantCulture);
        }

        [Test]
        public void CanParseExpected()
        {
            var giwen = "x^2 + 3.5xy + y = y^2 - xy + y";
            Equation actual = _target.Parse(giwen);

            var acttualStr = actual.ToString();

            Console.WriteLine(acttualStr);
        }
    }
}