﻿
using System.Collections.Generic;
using EquationTransformer.Model;
using NUnit.Framework;

namespace EquationTransformer.Tests
{
    [TestFixture]
    public class SummandFixture
    {
        [Test]
        public void WhenCallSummandToString_ThenReturnsExpected()
        {
            var summand = new Summand();
            var actual = summand.ToString();

            Assert.AreEqual("1", actual);
        }


        [Test]
        [TestCaseSource("TestCaseData")]
        public void WhenCallSummandToWithConstant_ThenReturnsExpected(object[] variables, string expected)
        {
            var summand = new Summand();

            foreach (object variable in variables)
            {
                var constant = variable as Constant;
                if (constant != null)
                {
                    summand.AppendVariable(constant);
                }
                var variable1 = variable as Variable;
                if (variable1 != null)
                {
                    summand.AppendVariable(variable1);
                }

            }

            var actual = summand.ToString();

            Assert.AreEqual(expected, actual);
        }

        private static object[] TestCaseData = {
            new object[]
            {
                new object[]
                {
                    new Constant(1),
                    new Variable('x', 2), 
                    new Variable('x', 2), 
                    new Constant(5)
                },
                "5x^4"
            },
            new object[]
            {
                new object[]
                {
                    new Constant(1),
                    new Variable('x', 2), 
                    new Variable('y', 2), 
                    new Variable('x', 2), 
                    new Variable('a'), 
                    new Variable('s'), 
                    new Constant(5),
                    new Variable('x', 2)
                },
                "5as(y^2)(x^8)"
            },
        };

    }
}