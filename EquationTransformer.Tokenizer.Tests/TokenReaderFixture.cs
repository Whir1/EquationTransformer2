﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace EquationTransformer.Tokenizer.Tests
{
    [TestFixture]
    public class TokenReaderFixture
    {
        private TokenReader _target;

        [SetUp]
        public void SetUp()
        {
            _target = new TokenReader(CultureInfo.InvariantCulture);
        }


        [Test]
        public void ParseEquation()
        {
            var eq = "x^2 + 3.5xy + y = y^2 - xy + y".Replace(" ", "");

            var tokens = _target.Read(eq);
            foreach (Token token in tokens)
            {
                Console.WriteLine(token);
            }

            var actualString = string.Join("", tokens.Select(c => c.Value));
            Assert.AreEqual("x^2+3.5xy+y=y^2-xy+y", actualString);
        }
    }
}
