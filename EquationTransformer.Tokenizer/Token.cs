﻿namespace EquationTransformer.Tokenizer
{
    public class Token
    {
        public TokenType TokenType;

        public string Value;

        public override string ToString()
        {
            return string.Format("TokenType: {0}, Value: {1}", TokenType, Value);
        }
    }
}