﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace EquationTransformer.Tokenizer
{
    /// <summary>
    /// Тиснул из одного из старых проектов, чуть чуть подрефакторил
    /// </summary>
    public class TokenReader
    {
        private readonly CultureInfo _cultureInfo;
        private readonly char _decimalSeparator;

        public TokenReader()
            : this(CultureInfo.InvariantCulture)
        {
        }

        public TokenReader(CultureInfo cultureInfo)
        {
            _cultureInfo = cultureInfo;
            _decimalSeparator = cultureInfo.NumberFormat.NumberDecimalSeparator[0];
        }

        /// <exception cref="ArgumentNullException"><paramref name="formula"/> is <see langword="null" />.</exception>
        /// <exception cref="TokenReaderException">Condition.</exception>
        public List<Token> Read(string formula)
        {
            if (string.IsNullOrEmpty(formula))
                throw new ArgumentNullException("formula");

            List<Token> tokens = new List<Token>();

            char[] characters = formula.ToCharArray();

            bool isFormulaSubPart = true;

            for (int i = 0; i < characters.Length; i++)
            {

                if (IsNumeric(characters[i], true, isFormulaSubPart))
                {
                    string buffer = "" + characters[i];

                    while (++i < characters.Length && IsNumeric(characters[i], false, isFormulaSubPart))
                    {
                        buffer += characters[i];
                    }

                    int intValue;
                    if (int.TryParse(buffer, out intValue))
                    {
                        tokens.Add(new Token { TokenType = TokenType.Numeric, Value = intValue.ToString()});
                        isFormulaSubPart = false;
                    }
                    else
                    {
                        float floatValue;
                        if (float.TryParse(buffer, NumberStyles.Float | NumberStyles.AllowThousands,
                            _cultureInfo, out floatValue))
                        {
                            tokens.Add(new Token { TokenType = TokenType.Numeric, Value = floatValue.ToString(_cultureInfo)});
                            isFormulaSubPart = false;
                        }
                    }

                    if (i == characters.Length)
                    {
                        continue;
                    }
                }

                if (IsLiteral(characters[i], true))
                {
                    string buffer = "" + characters[i];

                    while (++i < characters.Length && IsLiteral(characters[i], false))
                    {
                        buffer += characters[i];
                    }

                    tokens.Add(new Token { TokenType = TokenType.Text, Value = buffer});
                    isFormulaSubPart = false;

                    if (i == characters.Length)
                    {
                        continue;
                    }
                }
                tokens.Add(GetSeparatorToken(characters[i], i, out isFormulaSubPart));
            }

            return tokens;
        }

        private Token GetSeparatorToken(char c, int position, out bool isFormulaSubPart )
        {
            switch (c)
            {
                case '+':
                case '-':
                case '*':
                case '/':
                    isFormulaSubPart = true;
                    return new Token { TokenType = TokenType.Operation, Value = c.ToString() };
                case '^':
                    isFormulaSubPart = true;
                    return new Token { TokenType = TokenType.Power, Value = c.ToString() };
                case '(':
                    isFormulaSubPart = true;
                    return new Token { TokenType = TokenType.LeftBracket, Value = c.ToString() };
                case ')':
                    isFormulaSubPart = false;
                    return new Token { TokenType = TokenType.RightBracket, Value = c.ToString() };
                case '=':
                    isFormulaSubPart = false;
                    return new Token { TokenType = TokenType.Equal, Value = c.ToString() };
                default:
                    throw new TokenReaderException(String.Format("Invalid token \"{0}\" detected at position {1}.", c, position));
            }

        }

        private bool IsNumeric(char character, bool isFirstCharacter, bool isFormulaSubPart)
        {
            return character == _decimalSeparator || char.IsNumber(character) || (isFormulaSubPart && isFirstCharacter && character == '-');
        }

        private bool IsLiteral(char character, bool isFirstCharacter)
        {
            return char.IsLetter(character) || (!isFirstCharacter && char.IsNumber(character)) || (!isFirstCharacter && character == '_');
        }
    }
}