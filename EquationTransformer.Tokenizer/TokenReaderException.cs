﻿using System;

namespace EquationTransformer.Tokenizer
{
    public class TokenReaderException : Exception
    {
        public TokenReaderException(string message)
            : base(message)
        {

        }
    }
}