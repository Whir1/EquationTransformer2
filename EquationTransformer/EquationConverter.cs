﻿using System.Collections.Generic;
using EquationTransformer.Model;
using EquationTransformer.Parsers;
using EquationTransformer.Processors;

namespace EquationTransformer
{
    public class EquationConverter
    {
        private readonly IParser<Equation> _parser;
        private readonly ICollection<IEquationProcessor> _processors;

        public EquationConverter(IParser<Equation> parser, ICollection<IEquationProcessor> processors)
        {
            _parser = parser;
            _processors = processors;
        }

        public string ConvertToCanonical(string input)
        {
            Equation eq = _parser.Parse(input);
            foreach (IEquationProcessor equationProcessor in _processors)
            {
                eq = equationProcessor.ProcessEquation(eq);
            }
            return eq.ToString();
        }
    }
}