﻿using System;
using System.Globalization;

namespace EquationTransformer.Model
{
    public class Constant : VariableBase<float>
    {
        private readonly float _value;
        
        public override float Value { get { return _value; } }

        public Constant(float value)
            : this(value, CultureInfo.InvariantCulture)
        { }

        public Constant(float value, CultureInfo culture)
            : base(culture)
        {
            _value = value;
        }

        public override float GetRank()
        {
            return 1;
        }

        public bool IsZero()
        {
            return Math.Abs(_value) < Constants.Epsilon;
        }

        public bool IsPositive()
        {
            return _value >= 0;
        }

        public static Constant operator *(Constant left, Constant right)
        {
            return new Constant(left.Value * right.Value, left.Culture);
        }

        public static Constant operator +(Constant left, Constant right)
        {
            return new Constant(left.Value + right.Value, left.Culture);
        }

        public override string ToString()
        {
            return Value.ToString(Culture);
        }
    }
}