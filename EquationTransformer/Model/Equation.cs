﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EquationTransformer.Model
{
    public class Equation
    {
        private readonly List<Summand> _leftSummands;
        private readonly List<Summand> _rightSummands;

        public ICollection<Summand> Left { get { return _leftSummands;} }
        public ICollection<Summand> Right { get { return _rightSummands; } }

        public Equation()
        {
            _leftSummands = new List<Summand>();
            _rightSummands = new List<Summand>();
        }

        public Equation(ICollection<Summand> left, ICollection<Summand> right)
        {
            _leftSummands = left.ToList();
            _rightSummands = right.ToList();
        }

        public void AddToLeft(Summand newSummand)
        {
            _leftSummands.Add(newSummand);
        }

        public void AddToRight(Summand newSummand)
        {
            _rightSummands.Add(newSummand);
        }

        public override string ToString()
        {
            return String.Format("{0}{1}{2}", PrintPart(_leftSummands), Symbol.Equal, PrintPart(_rightSummands));
        }

        private string PrintPart(ICollection<Summand> parts)
        {
            var isFirst = true;
            var sb = new StringBuilder();
            foreach (Summand part in parts)
            {
                if (isFirst && part.Coefficient.IsPositive())
                {
                    sb.Append(part);
                }
                else
                {
                    sb.Append(part.Coefficient.IsPositive() ? "+" : "-");
                    sb.Append(part);
                }

                isFirst = false;
            }
            return sb.ToString();
        }
    }
}