﻿namespace EquationTransformer.Model
{
    public interface IVariable<out T>
    {
        T Value { get; }

        float GetRank();
    }
}