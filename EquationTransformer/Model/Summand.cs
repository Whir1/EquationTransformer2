﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace EquationTransformer.Model
{
    public class Summand
    {
        private Constant _coefficient;
        private readonly List<Variable> _variables;

        private IReadOnlyCollection<Variable> Variables { get { return _variables; } }

        public Summand():this(CultureInfo.InvariantCulture)
        { }

        public Summand(CultureInfo culture)
        {
            _coefficient = new Constant(1, culture);
            _variables = new List<Variable>();
        }

        public void AppendVariable<T>(IVariable<T> variable)
        {
            var constant = variable as Constant;
            if (constant != null)
            {
                Constant c = constant;
                _coefficient *= c;
                return;
            }

            var varaible = variable as Variable;
            if (varaible != null)
            {
                Variable similar = _variables.FirstOrDefault(c => c.IsSimilarTo(varaible));
                if (similar != null)
                {
                    int position = _variables.IndexOf(similar);
                    _variables[position] *= similar;
                }
                else
                {
                    _variables.Add(varaible);
                }
            }
        }

        public bool IsSimilarTo(Summand summand)
        {
            return Variables.SequenceEqual(summand.Variables);
        }

        public static Summand operator +(Summand left, Summand right)
        {
            if (!left.IsSimilarTo(right))
                throw new ArgumentException("Cant apply + operator to summand");
            
            var result = new Summand();
            result.AppendVariable(left.Coefficient + right.Coefficient);
            foreach (Variable variable in left.Variables)
            {
                result.AppendVariable(variable);
            }
            return result;
        }


        public Constant Coefficient { get { return _coefficient;} }

        public override string ToString()
        {
            if (_coefficient.IsZero())
            {
                return "0";
            }
            var sb = new StringBuilder();
            
            if (!_variables.Any())
            {
                sb.Append(_coefficient);
            }
            else
            {
                if (Math.Abs(_coefficient.Value) - 1 > Constants.Epsilon)
                {
                    sb.Append(_coefficient);
                }
                sb.Append(PrintVariables());
            }
            return sb.ToString();
        }

        private string PrintVariables()
        {
            var variables = _variables.OrderBy(c => c.Power)
                                      .ThenBy(c => c.Value);
            if (variables.Count() == 1)
            {
                return variables.First().ToString();
            }
            var sb = new StringBuilder();
            foreach (Variable variable in variables)
            {
                if (Math.Abs(variable.GetRank() - 1) > Constants.Epsilon)
                {
                    sb.AppendFormat("{0}{1}{2}", Symbol.BracketLeft, variable, Symbol.BracketRight);
                }
                else
                {
                    sb.Append(variable);
                }
            }
            return sb.ToString();
        }

        public float MaxRank()
        {
            return _variables.Max(c => c.Power);
        }
    }
}