﻿using System;
using System.Globalization;

namespace EquationTransformer.Model
{
    public class Variable : VariableBase<char>
    {
        private readonly char _value;

        public Variable(char name, float power = 1)
            : this(name, CultureInfo.InvariantCulture, power)
        { }

        public Variable(char name, CultureInfo culture, float power = 1)
            : base(culture)
        {
            _value = name;
            Power = power;
        }

        public float Power { get; set; }

        public override char Value
        {
            get { return _value; }
        }

        public override float GetRank()
        {
            return Power;
        }

        public bool IsSimilarTo(Variable other)
        {
            return Value == other.Value;
        }

        #region EqualByResharper
        protected bool Equals(Variable other)
        {
            return _value == other._value && Power.Equals(other.Power);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Variable)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_value.GetHashCode() * 397) ^ Power.GetHashCode();
            }
        }

        #endregion
        public static Variable operator *(Variable left, Variable right)
        {
            if (left.Value != right.Value)
                throw new ArgumentException("Cant apply multiplication operator to var with different names");
            return new Variable(left.Value, left.Culture, left.Power + right.Power);
        }

        public override string ToString()
        {
            if (Math.Abs(GetRank() - 1) > Constants.Epsilon)
            {
                return string.Format("{0}{1}{2}", Value, Symbol.Power, GetRank());
            }
            return Value.ToString();
        }
    }
}