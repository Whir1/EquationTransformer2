﻿using System.Globalization;

namespace EquationTransformer.Model
{
    public abstract class VariableBase<T> : IVariable<T>
    {
        protected CultureInfo Culture { get; private set; }

        protected VariableBase(CultureInfo culture)
        {
            Culture = culture;
        }

        public abstract T Value { get; }
        public abstract float GetRank();
    }
}