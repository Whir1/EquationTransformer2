﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using EquationTransformer.Model;
using EquationTransformer.Tokenizer;

namespace EquationTransformer.Parsers
{
    public class EquationParser : IParser<Equation>
    {
        private char[] _summandSplitters = new[]
                                           {
                                               Symbol.Plus,
                                               Symbol.Minus,
                                               Symbol.Equal
                                           };

        private char[] _summandSeparators = new[]
                                            {
                                                Symbol.Plus,
                                                Symbol.Minus,
                                                Symbol.Power,
                                                Symbol.Equal,
                                                Symbol.Multiplication
                                            };

        private readonly TokenReader _tokenReader;
        private readonly CultureInfo _culture;

        public EquationParser(TokenReader tokenReader, CultureInfo culture)
        {
            _tokenReader = tokenReader;
            _culture = culture;
        }

        public Equation Parse(string stringToParse)
        {
            string formatted = FormatString(stringToParse);

            var result = new Equation();

            List<Token> tokens = _tokenReader.Read(formatted);

            Token prevToken = null;
            var foundEqual = false;
            
            var summand = new Summand(_culture);
            for (var index = 0; index < tokens.Count; index++)
            {
                Token token = tokens[index];
                if (_summandSeparators.Contains(token.Value[0]))
                {
                    if (token.TokenType == TokenType.Power)
                    {
                        var nextToken = tokens[++index];
                        summand.AppendVariable(new Variable(prevToken.Value[0], _culture, float.Parse(nextToken.Value, _culture)));
                        prevToken = tokens[index - 1];
                    }
                    if (prevToken.TokenType == TokenType.Numeric)
                    {
                        summand.AppendVariable(new Constant(float.Parse(prevToken.Value, _culture)));
                    }
                    if (prevToken.TokenType == TokenType.Text && (token.TokenType == TokenType.Operation || token.TokenType == TokenType.Equal))
                    {
                        summand.AppendVariable(new Variable(prevToken.Value[0]));
                    }
                }
                if (prevToken!= null && prevToken.Value[0] == Symbol.Minus && token.TokenType == TokenType.Numeric)
                {
                    summand.AppendVariable(new Constant(-1));
                }
                
                if ((token.TokenType == TokenType.Operation ||token.TokenType == TokenType.Equal)  && _summandSplitters.Contains(token.Value[0]))
                {
                    if (foundEqual)
                    {
                        result.Right.Add(summand);
                    }
                    else
                    {
                        result.Left.Add(summand);
                    }
                    summand = new Summand(_culture);
                }
                if (token.TokenType == TokenType.Equal)
                {
                    foundEqual = true;
                }
                prevToken = token;
            }
            summand.AppendVariable(new Variable(tokens.Last().Value[0]));
            result.Right.Add(summand);
            return result;
        }

        private string FormatString(string stringToParse)
        {
            stringToParse = stringToParse.Replace(" ", "");
            
            var prevChar = ' ';
            var sb = new StringBuilder();
            foreach (char c in stringToParse)
            {
                
                if (char.IsLetter(prevChar) && char.IsLetter(c))
                {
                    sb.Append(Symbol.Multiplication);
                }
                if (char.IsDigit(prevChar) && char.IsLetter(c))
                {
                    sb.Append(Symbol.Multiplication);
                }
                if ((prevChar == Symbol.Minus || prevChar == Symbol.Plus) && char.IsLetter(c))
                {
                    sb.Append("1*");
                }

                sb.Append(c);
                prevChar = c;
            }
            return sb.ToString();
        }
    }
}