﻿namespace EquationTransformer.Parsers
{
    public interface IParser<out T>
    {
        T Parse(string stringToParse); 
    }
}