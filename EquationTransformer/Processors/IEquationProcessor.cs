﻿using EquationTransformer.Model;

namespace EquationTransformer.Processors
{
    public interface IEquationProcessor
    {
        Equation ProcessEquation(Equation equation);

        int Priority { get; }
    }
}