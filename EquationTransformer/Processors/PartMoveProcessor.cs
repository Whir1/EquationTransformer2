﻿using System.Collections.Generic;
using System.Globalization;
using EquationTransformer.Model;

namespace EquationTransformer.Processors
{
    public class PartMoveProcessor : IEquationProcessor
    {
        public Equation ProcessEquation(Equation equation)
        {
            var left = new List<Summand>(equation.Left);
            foreach (Summand summand in equation.Right)
            {
                summand.AppendVariable(new Constant(-1));
                left.Add(summand);
            }   
            
            var zeroSummand = new Summand();
            zeroSummand.AppendVariable(new Constant(0));
            var right = new List<Summand>() { zeroSummand };
            return new Equation(left, right);
        }

        public int Priority { get { return 1; } }
    }
}