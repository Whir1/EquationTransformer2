﻿using System;
using System.Collections.Generic;
using System.Linq;
using EquationTransformer.Model;

namespace EquationTransformer.Processors
{
    public class SummandReducerProcessor : IEquationProcessor
    {
        public Equation ProcessEquation(Equation equation)
        {
            ICollection<Summand> left = equation.Left;
            var optimizedLeft = OptimizePart(left);

            ICollection<Summand> right = equation.Right;
            var optimizedRight = OptimizePart(right);

            return new Equation(optimizedLeft, optimizedRight);
        }

        private ICollection<Summand> OptimizePart(ICollection<Summand> part)
        {
            var optimizedResult = new List<Summand>();

            foreach (Summand summand in part)
            {
                var similar = optimizedResult.FirstOrDefault(c => c.IsSimilarTo(summand));
                if (similar != null)
                {
                    int position = optimizedResult.IndexOf(similar);
                    optimizedResult[position] += summand;
                }
                else
                {
                    optimizedResult.Add(summand);
                }
            }

            var nonZeroSummands = optimizedResult.Where(c => Math.Abs(c.Coefficient.Value) > Constants.Epsilon)
                                                 .OrderByDescending(c => c.MaxRank()).ToList();

            if (!nonZeroSummands.Any())
            {
                var zero = new Summand();
                zero.AppendVariable(new Constant(0));
                nonZeroSummands.Add(zero);
            }
            return nonZeroSummands.ToList();
        }

        public int Priority { get { return 2; } }
    }
}