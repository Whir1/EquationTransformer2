﻿namespace EquationTransformer
{
    public class Symbol
    {
        public const char Plus = '+';
        public const char Minus = '-';
        public const char Equal = '=';
        public const char Multiplication = '*';
        public const char Division = '/';
        public const char Power = '^';
        public const char BracketLeft = '(';
        public const char BracketRight = ')'; 
    }
}